#!/bin/bash
# Usage: gromax-install.sh
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0
# Usage: gromax-install.sh --parameters


# Update ubuntu

echo "Update ubuntu"
sudo apt update -y

echo "Installing tools"
sudo apt-get -y install pdb2pqr \
                        python3-biopython \
                        python3-pip \
                        python3-tk \
                        python3-yaml \
                        git \
                        net-tools \
                        curl

echo "Installing Python pip modules.... GromacsWrapper, Numbpy and Others."
python3 -m pip install GromacsWrapper==0.8 numpy==1.18 scipy==1.4 matplotlib==3.0



echo "Get PARCE-1 code"
mkdir -p /opt/parce
cd /opt/parce
git clone https://github.com/PARCE-project/PARCE-1.git
cp -r PARCE-1/* .
rm -rf PARCE-1

ln -s /opt/parce/run_protocol.py /bin/parce

parce -h

echo "PARCE-1 Install Completed"

