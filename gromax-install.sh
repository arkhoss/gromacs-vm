#!/bin/bash
# Usage: gromax-install.sh
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0
# Usage: gromax-install.sh --parameters


# Update ubuntu


GROMACS_YEAR=2020

echo "Update ubuntu"
sudo apt update -y

echo "Installing tools"
sudo apt-get -y install build-essential \
                libfftw3-dev \
                software-properties-common

echo "setup python3"
sudo add-apt-repository ppa:deadsnakes/ppa -y

sudo apt update -y
sudo apt-get -y install python3.8.5

sudo unlink /usr/bin/python3

ln -s /usr/bin/python3.8 /usr/bin/python3

python3 ––version

echo "Installing cmake"
CMAKE_VERSION=3.19
CMAKE_BUILD=1

cd /tmp
wget https://cmake.org/files/v$CMAKE_VERSION/cmake-$CMAKE_VERSION.$CMAKE_BUILD.tar.gz
tar -xzvf cmake-$CMAKE_VERSION.$CMAKE_BUILD.tar.gz
cd cmake-$CMAKE_VERSION.$CMAKE_BUILD/

./bootstrap -- -DCMAKE_USE_OPENSSL=OFF
make -j$(nproc)
sudo make install

cmake --version

cd /tmp

wget ftp://ftp.gromacs.org/gromacs/gromacs-${GROMACS_YEAR}.tar.gz
tar xvzf gromacs-${GROMACS_YEAR}.tar.gz

wget https://ftp.gromacs.org/regressiontests/regressiontests-${GROMACS_YEAR}.tar.gz
tar xvzf regressiontests-${GROMACS_YEAR}.tar.gz

sudo mkdir -p /tmp/gromacs-${GROMACS_YEAR}/build

cd /tmp/gromacs-${GROMACS_YEAR}/build

echo "building gromacs..."
sudo cmake .. -DGMX_BUILD_OWN_FFTW=OFF \
              -DREGRESSIONTEST_DOWNLOAD=OFF \
              -DCMAKE_C_COMPILER=gcc \
              -DGMX_GPU=off \
              -DREGRESSIONTEST_PATH=/tmp/regressiontests-${GROMACS_YEAR}

echo "checking gromacs..."
sudo make check
echo "installing gromacs..."
sudo make install

source /usr/local/gromacs/bin/GMXRC

PATH=/usr/local/gromacs/bin:${PATH}

sudo ln -s /usr/local/gromacs/bin/gmx /bin/gmx

echo "Gromacs Installed"


