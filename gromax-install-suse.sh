#!/bin/bash
# Usage: gromax-install.sh
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0
# Usage: gromax-install.sh --parameters

GROMACS_YEAR=2020


echo "Installing tools"
sudo zypper install -t pattern devel_basis
wget https://rpmfind.net/linux/opensuse/tumbleweed/repo/oss/x86_64/fftw3-devel-3.3.9-2.1.x86_64.rpm
sudo zypper in fftw3-devel-3.3.9-2.1.x86_64.rpm
rm -rf fftw3-devel-3.3.9-2.1.x86_64.rpm

echo "Installing gromacs"
sudo zypper addrepo https://download.opensuse.org/repositories/openSUSE:Factory/standard/openSUSE:Factory.repo
sudo zypper refresh
sudo zypper install gromacs

gmx --version
echo "Gromacs Installed"

